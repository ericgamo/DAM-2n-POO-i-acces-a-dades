package mongodb;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;

public class ExempleIndexos {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("punts");
		MongoCollection<Document> coll2 = db.getCollection("punts2");
		
		// Primera execució: crea les col·leccions
		/*coll.drop();
		coll2.drop();
		
		for (int x=0; x<1000; x++) {
			for (int y=0; y<1000; y++) {
				coll.insertOne(new Document("x",x).append("y", y));
				coll2.insertOne(new Document("x",x).append("y", y));
			}
			if (x%10==0)
				System.out.print(".");
		}
		System.out.println();
		
		coll.createIndex(new Document("y",1), (new IndexOptions()).name("index_y"));
		*/
		
		// Segona execució: test amb índex
		long startTime = System.currentTimeMillis();
		coll.find(Filters.eq("y", 500)).forEach((Document doc)->{});
		long endTime = System.currentTimeMillis();
		System.out.println("Execució amb índex: "+(endTime-startTime));

		// Tercera execució: test sense índex
		/*long startTime = System.currentTimeMillis();
		coll2.find(Filters.eq("y", 500)).forEach((Document doc)->{});
		long endTime = System.currentTimeMillis();
		System.out.println("Execució sense índex: "+(endTime-startTime));
		*/
		client.close();
	}

}
