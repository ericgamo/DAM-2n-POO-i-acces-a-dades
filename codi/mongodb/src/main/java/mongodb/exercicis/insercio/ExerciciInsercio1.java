package mongodb.exercicis.insercio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExerciciInsercio1 {

	public static void main(String[] args) {
		int id, pop;
		String city, state;
		double lat, lon;
		String line;
		String[] parts;
		List<Document> zips = new ArrayList<Document>();
		long importedDocs;
		long endTime;
		long startTime = System.currentTimeMillis();
		try (BufferedReader reader = new BufferedReader(new FileReader("zips.json"))) {
			while ((line=reader.readLine())!=null) {
				parts = line.split("\"");
				id = Integer.parseInt(parts[3]);
				city = parts[7];
				state = parts[15];
				pop = Integer.parseInt(parts[12].replace(":", "").replace(",", "").trim());
				parts = parts[10].split(" ");
				lat = Double.parseDouble(parts[3].replace(",", "").trim());
				lon = Double.parseDouble(parts[4].trim());
				
				zips.add(new Document("_id", id).append("city", city).append("loc", Arrays.asList(lat, lon))
						.append("pop", pop).append("state", state));
			}
			MongoClient client = new MongoClient();
			MongoDatabase db = client.getDatabase("exemples");
			MongoCollection<Document> coll = db.getCollection("zips");
			
			coll.drop();
			coll.insertMany(zips);
			importedDocs = coll.count();
			client.close();
			endTime = System.currentTimeMillis();
			System.out.println("Registres importats: "+importedDocs+" - Temps: "+(endTime-startTime)+" ms");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
