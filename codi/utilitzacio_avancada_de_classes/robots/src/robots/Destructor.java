package robots;

public class Destructor extends MarkJava {

	@Override
	public boolean decideixSiMou() {
		if (super.decideixSiMou()) {
			if (obteEnergia() >= 5 && random.nextInt(10)+1 <= 4)
				return true;
		}
		return false;
	}

	@Override
	public void interactua(MarkJava unAltreRobot) {
		if (obteEnergia() >= 5) {
			if (unAltreRobot instanceof Destructor) {
				if (random.nextInt(2) == 0)
					unAltreRobot.gastaBateria();
			} else
				unAltreRobot.gastaBateria();
			gastaEnergia(3);
		}
	}

}
