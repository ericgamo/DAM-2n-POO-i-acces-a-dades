package zoo;

public class Vaca extends Animal {

	@Override
	public String alimenta(Zoo z) {
		return "Una vaca comença a pastar amb tranquilitat";
	}

	@Override
	public String expressa(Zoo z) {
		return "Una vaca fa mu";
	}

	@Override
	public String mou(Zoo z) {
		return "Una vaca es posa a dormir";
	}

}
