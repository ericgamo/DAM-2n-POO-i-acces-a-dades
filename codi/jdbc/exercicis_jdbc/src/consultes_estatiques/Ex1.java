package consultes_estatiques;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//1. Fes un programa que mostri el nom i descripció de totes les pel·lícules 
//que duren més de dues hores i mitja.
public class Ex1 {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "usbw";
		String sql = "SELECT title, description FROM film WHERE length > 150";
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement();
				ResultSet rs = statement.executeQuery(sql);) {
			while (rs.next())
				System.out.println(rs.getString(1)+" -- "+rs.getString(2));
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
