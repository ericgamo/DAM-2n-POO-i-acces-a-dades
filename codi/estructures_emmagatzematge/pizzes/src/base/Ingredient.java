package base;

public interface Ingredient {
	public String recepta();
	public double getPreu();
	public String descripcio();
	public Ingredient clone() throws CloneNotSupportedException;
}
