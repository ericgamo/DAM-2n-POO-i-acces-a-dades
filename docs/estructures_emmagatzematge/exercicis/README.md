### Exercicis

 * [Exercicis de piles i cues](piles_i_cues.md)
 * [Exercicis de llistes](llistes.md)
 * [Exercicis de conjunts i diccionaris](conjunts_i_diccionaris.md)
 * [Exercici del domino](domino.md)

#### Exercicis d'utilització avançada de classes i estructures d'emmagatzematge

 * [Exercici de la fàbrica de naus](fabrica_naus.md)
 * [Exercici de la pizzera d'en Luigi](pizzeria.md)
