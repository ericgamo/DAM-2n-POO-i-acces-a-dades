## L'*aggregation framework*

L'*aggregation framework* ens permet realitzar una colla d'operacions
avançades que seria complicat fer a mà:

- Agrupar consultes de diversos documents.
- Realitzar operacions de recompte i estadística sobre els resultats obtinguts.

Essencialment, l'*aggregation framework* ens proporciona les funcionalitats que
en SQL tenim amb el *JOIN*, el *GROUP BY*, i diverses funcions de recompte
com *count* o *sum*.

### El *pipeline*

L'*aggregation framework* utilitza el concepte de **canonada** (*pipeline*).
La idea és que una consulta es divideix en diversos **estadis** (*stages*) i
que els resultats d'un estadis es passen al següent estadi, seguint un ordre
preestablert.

A la següent taula podem veure un resum dels estadis més habituals que hi ha i
la seva funcionalitat:

| Nom de l'estadi | Ús | Multiplicitat |
| ----------------- | ---- | ---- |
| $project | Selecciona els camps que volem d'un document i els mapeja als camps del document de sortida. | [1, 1] |
| $match | Filtra els documents que ens interessen utilitzant els filtres habituals de MongoDB | [1, 0-1] |
| $limit | Retorna només la quantitat de documents demanada | [1, 0-1] |
| $skip | Es salta el nombre de documents indicat abans de retornar resultats. | [1, 0-1] |
| $unwind | Divideix un array dels documents d'entrada, obtenint un document per a cada valor de l'array. | [1, 0-n] |
| $group | Agrupa els documents per algun camp i permet fer calculs per a cada grup obtingut. | [n, 1] |
| $sort | Ordena els documents per algun camp | [1, 1] |
| $lookup | Permet unir documents de dues col·leccions diferents. | [1, 1] |
| $out | Reescriu el resultat de tot el procés a una nova col·lecció. | [1, 1] |

Totes les consultes que utilitzen l'*aggregation framework* es creen
amb la funció *aggregate*:

```
db.zips.aggregate(...)
```

*aggregate* rep un array de documents. Cada document conté una única parella
de clau-valor. La clau és l'estadi del *pipeline* a utilitzar, i el valor
els seus arguments:

```
db.zips.aggregate([
  {$project : ...},
  {$limit : ...}
])
```

Existeixen multitud d'operadors que es poden utilitzar dins de l'*aggregation
framework*, com operadors booleans (*$and*, *$or*...), de comparació (*$eq*,
*$gt*...), aritmètics (*$add*...), de cadenes (*$concat*...),
d'arrays (*$filter*, *$size*, *$push*...), etc.

Pots veure un resum amb tots els operadors a
https://docs.mongodb.org/manual/reference/operator/aggregation/#expression-operators

### Estadis

Anirem provant cadascun dels estadis amb diversos exemples sobre la
col·lecció *zips*.

Recordem l'aspecte dels documents d'aquesta col·lecció:

```
> db.zips.findOne()
{
	"_id" : "01001",
	"city" : "AGAWAM",
	"loc" : [
		-72.622739,
		42.070206
	],
	"pop" : 15338,
	"state" : "MA"
}
```

#### *$project*

La projecció en l'*aggregation framework* funciona igual que en les consultes
simples: indiquem un 1 per seleccionar un camp o un 0 per excloure'l. L'*_id*
se selecciona per defecte.

Ens quedem només amb els camps *pop*, *state* i l'*_id*:

```
> db.zips.aggregate([
  {$project : {pop:1, state:1}}
])
{ "_id" : "01001", "pop" : 15338, "state" : "MA" }
{ "_id" : "01007", "pop" : 10579, "state" : "MA" }
{ "_id" : "01002", "pop" : 36963, "state" : "MA" }
{ "_id" : "01008", "pop" : 1240, "state" : "MA" }
...
```

#### *$match*

Els filtres també funcionen igual que en les consultes simples.

Seleccionem els codis postals que tenen una població menor a 50 habitants:

```
> db.zips.aggregate([
  {$match : {pop : {$lt:50}}}
])
{ "_id" : "01338", "city" : "BUCKLAND", "loc" : [ -72.764124, 42.615174 ], "pop" : 16, "state" : "MA" }
{ "_id" : "02163", "city" : "CAMBRIDGE", "loc" : [ -71.141879, 42.364005 ], "pop" : 0, "state" : "MA" }
{ "_id" : "02815", "city" : "CLAYVILLE", "loc" : [ -71.670589, 41.777762 ], "pop" : 45, "state" : "RI" }
...
```

Fem la mateixa selecció d'abans, però només ens interessa el codi postal, la
ciutat i l'estat:

```
> db.zips.aggregate([
  {$match : {pop : {$lt:50}}},
  {$project : {city:1, state:1}}
])
{ "_id" : "01338", "city" : "BUCKLAND", "state" : "MA" }
{ "_id" : "02163", "city" : "CAMBRIDGE", "state" : "MA" }
{ "_id" : "02815", "city" : "CLAYVILLE", "state" : "RI" }
...
```

#### *$limit*

Volem veure només tres codis postals:

```
> db.zips.aggregate([
  {$limit : 3}
])
{ "_id" : "01001", "city" : "AGAWAM", "loc" : [ -72.622739, 42.070206 ], "pop" : 15338, "state" : "MA" }
{ "_id" : "01007", "city" : "BELCHERTOWN", "loc" : [ -72.410953, 42.275103 ], "pop" : 10579, "state" : "MA" }
{ "_id" : "01002", "city" : "CUSHMAN", "loc" : [ -72.51565, 42.377017 ], "pop" : 36963, "state" : "MA" }
```

Unim els exemples anteriors: volem els codis postals, el nom de les
poblacions i els seus estats, de tres codis que tinguin menys de 50 habitants:

```
> db.zips.aggregate([
  {$match : {pop : {$lt:50}}},
  {$project : {city:1, state:1}},
  {$limit : 3}
])
{ "_id" : "01338", "city" : "BUCKLAND", "state" : "MA" }
{ "_id" : "02163", "city" : "CAMBRIDGE", "state" : "MA" }
{ "_id" : "02815", "city" : "CLAYVILLE", "state" : "RI" }
```

#### *$sort*

Volem obtenir la llista de documents ordenats de més a menys població:

```
> db.zips.aggregate([
  {$sort : {pop:-1}}
])
{ "_id" : "60623", "city" : "CHICAGO", "loc" : [ -87.7157, 41.849015 ], "pop" : 112047, "state" : "IL" }
{ "_id" : "11226", "city" : "BROOKLYN", "loc" : [ -73.956985, 40.646694 ], "pop" : 111396, "state" : "NY" }
{ "_id" : "10021", "city" : "NEW YORK", "loc" : [ -73.958805, 40.768476 ], "pop" : 106564, "state" : "NY" }
```

Ara volem els tres codis postals amb més població, dins dels que tenen menys
de 50 habitants. De cada codi volem el codi, la ciutat, l'estat i la
població:

```
> db.zips.aggregate([
  {$match : {pop:{$lt:50}}},
  {$project : {city:1, state:1, pop:1}},
  {$sort : {pop:-1}},
  {$limit : 3}
])
{ "_id" : "15763", "city" : "NORTHPOINT", "pop" : 49, "state" : "PA" }
{ "_id" : "11371", "city" : "FLUSHING", "pop" : 49, "state" : "NY" }
{ "_id" : "08327", "city" : "LEESBURG", "pop" : 49, "state" : "NJ" }
```

**Atenció**: l'ordre és important. No funciona bé:

```
> db.zips.aggregate([
  {$match : {pop:{$lt:50}}},
  {$project : {city:1, state:1, pop:1}},
  {$limit : 3},
  {$sort : {pop:-1}}
])
{ "_id" : "02815", "city" : "CLAYVILLE", "pop" : 45, "state" : "RI" }
{ "_id" : "01338", "city" : "BUCKLAND", "pop" : 16, "state" : "MA" }
{ "_id" : "02163", "city" : "CAMBRIDGE", "pop" : 0, "state" : "MA" }
```

En aquest cas, primer s'han agafat tres resultats i després s'han ordenat.

#### *$skip*

Aquesta és la mateixa consulta d'abans, però ens saltem 10 codis abans de
seleccionar-ne 3:

```
> db.zips.aggregate([
  {$match : {pop:{$lt:50}}},
  {$project : {city:1, state:1, pop:1}},
  {$sort : {pop:-1}},
  {$skip : 10},
  {$limit : 3}
])
{ "_id" : "79326", "city" : "FIELDTON", "pop" : 48, "state" : "TX" }
{ "_id" : "57545", "city" : "KEYAPAHA", "pop" : 48, "state" : "SD" }
{ "_id" : "14748", "city" : "KILL BUCK", "pop" : 47, "state" : "NY" }
```

#### *$unwind*

L'*unwind* és una mica més complex que els estadis que hem vist fins ara.
Permet dividir un document que tingui un array en diversos documents. Cadascun
dels nous documents tindrà només un dels valors de l'array. Així, si un
document té tres elements a l'array que utilitzem per fer l'*unwind*, es
convertirà en tres documents a la resposta.

En aquest exemple dividim cada document en dos, un que conté la latitud i un
altre que conté la longitud:

```
> db.zips.aggregate([
  {$unwind : "$loc"},
  {$sort : {_id:1}},
  {$limit:4}
])
{ "_id" : "01001", "city" : "AGAWAM", "loc" : -72.622739, "pop" : 15338, "state" : "MA" }
{ "_id" : "01001", "city" : "AGAWAM", "loc" : 42.070206, "pop" : 15338, "state" : "MA" }
{ "_id" : "01002", "city" : "CUSHMAN", "loc" : 42.377017, "pop" : 36963, "state" : "MA" }
{ "_id" : "01002", "city" : "CUSHMAN", "loc" : -72.51565, "pop" : 36963, "state" : "MA" }
```

També podem conservar la posició de l'array on s'ha trobat cadascuna de les
dades. Si ho fem en l'exemple anterior, serà possible saber si el nombre que
tenim a *loc* es refereix a la longitud o a la latitud:

```
> db.zips.aggregate([
  {$unwind : {path:"$loc", includeArrayIndex:"latlong"}},
  {$sort : {_id:1, latlong:1}},
  {$limit : 4}
])
{ "_id" : "01001", "city" : "AGAWAM", "loc" : -72.622739, "pop" : 15338, "state" : "MA", "latlong" : NumberLong(0) }
{ "_id" : "01001", "city" : "AGAWAM", "loc" : 42.070206, "pop" : 15338, "state" : "MA", "latlong" : NumberLong(1) }
{ "_id" : "01002", "city" : "CUSHMAN", "loc" : -72.51565, "pop" : 36963, "state" : "MA", "latlong" : NumberLong(0) }
{ "_id" : "01002", "city" : "CUSHMAN", "loc" : 42.377017, "pop" : 36963, "state" : "MA", "latlong" : NumberLong(1) }
```

#### *$group*

L'estadi *group* s'assembla molt al *group by* de SQL. Quan utilitzem *group*
hem de definir quins seran els camps dels nous documents i com s'han de
calcular. Com a mínim hem d'indicar quin camp serà el nou *_id*.

En aquest exemple creem uns nous documents que tenen com a clau l'estat, i
en seleccionem els cinc primers alfabèticament:

```
> db.zips.aggregate([
  {$group : {_id:"$state"}},
  {$sort : {_id:1}},
  {$limit : 5}
])
{ "_id" : "AK" }
{ "_id" : "AL" }
{ "_id" : "AR" }
{ "_id" : "AZ" }
{ "_id" : "CA" }
```

Ara refinem l'agrupació anterior i hi afegim la suma de tota la població de
tots els codis postals que pertanyen a cadascun dels estats. Ens quedem
amb els 5 estats que tenen més població:

```
> db.zips.aggregate([
  {$group : {_id:"$state", pop:{$sum:"$pop"}}},
  {$sort : {pop:-1}},
  {$limit : 5}
])
{ "_id" : "CA", "pop" : 29754890 }
{ "_id" : "NY", "pop" : 17990402 }
{ "_id" : "TX", "pop" : 16984601 }
{ "_id" : "FL", "pop" : 12686644 }
{ "_id" : "PA", "pop" : 11881643 }
```

### Operadors

Anem a veure alguns operadors específics que són habituals en les consultes
fetes utilitzant l'*aggregation framework*.

#### *$push*

L'operador *$push* afegeix valors a un array.

En aquest exemple, volem obtenir una llista amb totes les ciutats d'EEUU i
que cadascuna contingui la llista amb tots els seus codis postals:

```
> db.zips.aggregate([
  {$group: {
    _id: {city:"$city",state:"$state"},
    zips: {$push:"$_id"}
  }}
])
...
{ "_id" : { "city" : "BURIEN", "state" : "WA" }, "zips" : [ "98146" ] }
{ "_id" : { "city" : "SEATTLE", "state" : "WA" }, "zips" : [ "98101", "98102", "98103", "98104", "98105", "98106", "98107", "98109", "98112", "98115", "98116", "98117", "98118", "98119", "98121", "98122", "98125", "98126", "98133", "98134", "98136", "98144", "98177", "98199" ] }
{ "_id" : { "city" : "SNOQUALMIE", "state" : "WA" }, "zips" : [ "98065" ] }
{ "_id" : { "city" : "REDMOND", "state" : "WA" }, "zips" : [ "98052", "98053" ] }
{ "_id" : { "city" : "RAVENSDALE", "state" : "WA" }, "zips" : [ "98051" ] }
{ "_id" : { "city" : "KENT", "state" : "WA" }, "zips" : [ "98031", "98032", "98042" ] }
...
```

L'operador *$push* actua com un invert de l'estadi *$unwind*: l'últim permet
dividir un array en diversos documents, mentre que el primer permet unir
diversos documents en un array.

#### *$slice*

L'operador *$slice* permet quedar-se només amb alguns elements d'un array.

Imaginem que a l'exemple anterior volem obtenir un màxim de tres codis postals
per a cada ciutat.

Un cop tenim la consulta anterior feta, podem afegir un estadi *$project*
que ens seleccioni només tres elements de cada array:

```
db.zips.aggregate([
{$group:{
	_id:{city:"$city",state:"$state"},
	zips:{$push:"$_id"},
}},
{$project:{zips:{$slice:["$zips",3]}}}
])
```

#### *$addToSet*

L'operador *$addToSet* és molt similar a *$push*, també afegeix valors a un
array, però només ho fa si els valors no són ja dins de l'array. És a dir,
*$addToSet* permet crear arrays sense valors duplicats.

Imaginem que a partir de la col·lecció de codis postals volem obtenir, per a
cada estat, una lista amb les seves ciutats. Naturalment, no volem ciutats
repetides a dins de cada estat.

Al final, volem obtenir una col·lecció que tingui per *_id* el codi de l'estat
i que inclogui un array amb totes les ciutat d'aquell estat:

```
> db.zips.aggregate([
  {$group: {
    _id: "$state",
    cities: {$addToSet:"$city"}
  }}
])
{ "_id" : "CA", "cities" : [ "SOUTH LAKE TAHOE", "TAHOE CITY", "TAHOMA", "PENINSULA VILLAG", "WENDEL", "VINTON", "STANDISH", "SIERRAVILLE", "SIERRA CITY", "CALPINE", "RAVENDALE", "MILFORD", "LOYALTON", "LITCHFIELD", "LAKE CITY",...
{ "_id" : "MT", "cities" : [ "WHITEFISH", "PROCTOR", "POLEBRIDGE", "MARION", "LAKESIDE", "KILA", "COLUMBIA FALLS", "SWAN LAKE", "EVERGREEN", "BIG ARM", "VICTOR", "SUPERIOR", "SULA", "SEELEY LAKE", "SAINT IGNATIUS", "POLSON", "PLAINS", "NOXON", "NIARADA", "HUSON", "HOT SPRINGS",...
```

Fixeu-vos que en l'exemple de la secció anterior, com els codis *_id* són
únics a la col·lecció original, el resultat és el mateix si utilitzem *$push*
com *$addToSet*, però no passa el mateix en aquest últim exemple, perquè, amb
*$push*, les ciutats que tenen més d'un codi postal haurien sortit repetides.

### L'*aggregation framework* en Java

Un cop tenim clar com utilitzar l'*aggregation framework* en la línia de
comandes del MongoDB, fer-ho en Java és senzill: només hem de traduir els
nostres documents JSON a l'estructura de *Document* del driver de Java.

Per exemple, si volem consultar quins estats d'EEUU tenen més d'un milió
d'habitants, podríem utilitzar la següent consulta:

```
db.zips.aggregate( [
	{ $group:
		{ _id: "$state",totalPop:
			{ $sum: "$pop" }
		}
	},
	{ $match:
		{ totalPop:
			{ $gte: 10000000 }
		}
	}
] )
```

El següent programa executa aquesta mateixa consulta des de Java:

```java
MongoClient client = new MongoClient();
MongoDatabase db = client.getDatabase("test");
MongoCollection<Document> coll = db.getCollection("zips");

List<Document> pipeline = Arrays.asList(
		new Document("$group",
				new Document("_id", "$state").append("totalPop",
						new Document("$sum", "$pop")
				)
		),
		new Document("$match",
				new Document("totalPop",
						new Document("$gte", 10000000)
				)
		)
);

List<Document> results = coll.aggregate(pipeline)
		.into(new ArrayList<Document>());
for (Document doc : results) {
	System.out.println(doc.toJson());
}
client.close();
```

El resultat d'executar-lo és:

```
{ "_id" : "CA", "totalPop" : 29754890 }
{ "_id" : "FL", "totalPop" : 12686644 }
{ "_id" : "PA", "totalPop" : 11881643 }
{ "_id" : "NY", "totalPop" : 17990402 }
{ "_id" : "OH", "totalPop" : 10846517 }
{ "_id" : "IL", "totalPop" : 11427576 }
{ "_id" : "TX", "totalPop" : 16984601 }
```

Si ens resulta més fàcil treballar amb consultes JSON que en objectes Java,
podem realitzar les consultes directament en JSON:

```java
List<Document> pipeline = Arrays.asList(
		Document.parse("{$group: {_id: \"$state\", totalPop: { $sum: \"$pop\" }}}"),
  	Document.parse("{$match: {totalPop:	{$gte: 10000000}}}")
);
```

Hem de vigilar a protegir les cometes que hi puguin haver!

Com en els casos anteriors, tenim una classe que ens facilita la construcció
d'agregats a partir de mètodes *static*. Aquesta classe s'anomena
*Aggregates* i s'utilitza conjuntament amb altres classes constructores com
*Filters* o *Accumulators*.

La mateixa consulta escrita seguint aquest sistema queda així:

```java
coll.aggregate(Arrays.asList(
		Aggregates.group("$state", Accumulators.sum("totalPop", "$pop")),
		Aggregates.match(Filters.gte("totalPop", 1000000))
));
```

I afegint els *import static*:

```java
coll.aggregate(asList(
		group("$state", sum("totalPop", "$pop")),
		match(gte("totalPop", 1000000))
));
```
