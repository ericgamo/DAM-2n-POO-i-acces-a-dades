#### La interfície Comparable

Suposem que volem implementar un algorisme d'ordenació, i ho fem en una
classe anomenada *Algorismes*. Hem triat un algorisme senzill
d'implementar i d'entendre, anomenat *ordenació per inserció*, que és
útil quan les dades del vector ja estan bastant ordenades d'entrada. El
codi seria així:

```java
    public static void sort(int[] vector) {
       int clau;
       int acanviar, posicio;

       for (posicio=1; posicio<vector.length; posicio++) {
           clau = vector[posicio];
           acanviar = posicio - 1;
           while (acanviar >= 0 && vector[acanviar] > clau) {
               vector[acanviar+1] = vector[acanviar];
               acanviar = acanviar - 1;
           }
           vector[acanviar+1] = clau;
       }
   }
```

Amb això hem aconseguit tenir un algorisme per ordenar vectors d'enters.
Però, què passa si el nostre vector és de nombres reals? Hauríem de fer
un altre mètode similar:

```java
    public static void sort(double[] vector) {
       double clau;
       int acanviar, posicio;

       for (posicio=1; posicio<vector.length; posicio++) {
           clau = vector[posicio];
           acanviar = posicio - 1;
           while (acanviar >= 0 && vector[acanviar] > clau) {
               vector[acanviar+1] = vector[acanviar];
               acanviar = acanviar - 1;
           }
           vector[acanviar+1] = clau;
       }
   }
```

Fixeu-vos que els dos mètodes són idèntics, només ha canviat el tipus de
dades de vector i el de clau.

Però anem a complicar-ho una mica més. Suposem que tenim una classe
*Data* que emmagatzema una determinada data a una determinada part del
món. Les dates es poden ordenar si tenim un bon criteri de quina data va
abans que quina altra. Per exemple, podríem passar les dates al mateix
fus horari (UTC), comptar els segons que han passat des que va començar
el 1970, i això ens donaria un nombre enter per cada data que seria
senzill de comparar.

Si tenim un mètode *getTemps* a la classe *Data* que ens retorna el
nombre desitjat, l'algorisme quedaria així:

```java
    public static void sort(Data[] vector) {
       Data clau;
       int acanviar, posicio;

       for (posicio=1; posicio<vector.length; posicio++) {
           clau = vector[posicio];
           acanviar = posicio - 1;
           while (acanviar>=0 && vector[acanviar].getTemps()>clau.getTemps()) {
               vector[acanviar+1] = vector[acanviar];
               acanviar = acanviar - 1;
           }
           vector[acanviar+1] = clau;
       }
   }
```

Amb això anirem fent, i crearíem un algorisme d'ordenació per als vector
de cada classe els objectes de la qual tingui sentit que es puguin
ordenar.

A banda d'haver de fer multitud de mètodes diferents per a les nostres
ordenacions, el problema amb aquest enfocament és que cada vegada que es
creï una nova classe, caldrà crear un nou mètode per ordenar, cosa que
ens obligarà a modificar la classe *Algorismes*.

Però aquesta feina és molt absurda, perquè realment el nostre algorisme
sempre és el mateix. Només canvien dues coses: el tipus de dades i la
forma de comparar-les entre elles.

Primer de tot anem a resoldre el problema de la comparació, per obtenir
un algorisme únic. Llavors tractarem el problema del tipus de dades.

Doncs anem a seguir dues de les màximes de la programació orientada a
objectes: *separa aquelles coses que varien de les que són constants*
(ho farem ***encapsulant allò que varia***) i ***fes que cada classe sigui
responsable de sí mateixa***.

En el nostre exemple, qui és responsable de saber com es comparen dues
dates? Naturalment, la classe *Data* és qui ha de saber això, no pas la
classe *Algorisme*. Igualment, la classe *Integer* serà responsable de
comparar nombres enters, i així successivament.

Aleshores, creem un mètode a la classe *Data* per comparar dates. Podem
fer, per exemple, que aquest mètode retorni un nombre negatiu si la
primera data és menor que la segona, positiu si la segona és menor que
la primera, i 0 si són idèntiques. El codi quedaria:

```java
    public int compareTo(Data arg0) {
       return getTemps() - arg0.getTemps();
   }
```

D'aquesta manera, el nostre mètode d'ordenació seria:

```java
    public static void sort(Data[] vector) {
       Data clau;
       int acanviar, posicio;

       for (posicio=1; posicio<vector.length; posicio++) {
           clau = vector[posicio];
           acanviar = posicio - 1;
           while (acanviar >= 0 && vector[acanviar].compareTo(clau) > 0) {
               vector[acanviar+1] = vector[acanviar];
               acanviar = acanviar - 1;
           }
           vector[acanviar+1] = clau;
       }
   }
```

Fixeu-vos que ara el nostre algorisme funcionaria per a qualsevol classe
que tingués un mètode *compareTo* que segueixi aquestes regles, de
manera que el codi seria idèntic tant si comparem dates, com enters, o
qualsevol altre classe comparable.

Només ens queda arreglar el tema dels tipus. No podem utilitzar *Object*
com a classe genèrica, perquè no tots els object tenen un mètode
*compareTo*.

D'alguna manera hem de dir que *Integer*, *Double* i *Data* tenen
quelcom en comú, i no ho podem fer per herència perquè no té sentit que
una data derivi d'un nombre.

La solució és, evidentment, capturar el què tenen en comú totes aquestes
classes en una interfície, i assegurar que totes les classes que
implementin aquesta interfície tindran el mètode *compareTo*.

La interfície seria alguna cosa com:

```java
public interface Comparable {
   public int compareTo(Object arg0);
}
```

I *Data*, *Integer* i *Double* implementarien aquesta interfície:

```java
    public class Data implements Comparable
```

El codi gairebé definitiu per l'algorisme d'ordenació seria:

```java
    public static void sort(Comparable[] vector) {
       Comparable clau;
       int acanviar, posicio;

       for (posicio=1; posicio<vector.length; posicio++) {
           clau = vector[posicio];
           acanviar = posicio - 1;
           while (acanviar >= 0 && vector[acanviar].compareTo(clau) > 0) {
               vector[acanviar+1] = vector[acanviar];
               acanviar = acanviar - 1;
           }
           vector[acanviar+1] = clau;
       }
   }
```

El codi anterior ja funciona perfectament, però encara el podem millorar
una mica. Aquí estem assegurant que rebrem un vector d'objectes i tots
ells implementen la interfície *Comparable*, però podria ser que hi
haguessin enters i dates al vector, i no té sentit comparar un enter amb
una data.

Podem millorar la seguretat del codi utilitzant els genèrics, i
especificant que les classes es poden comparar només amb algunes altres
classes, però no amb qualsevol.

Fent-ho així, la interfície quedaria:

```java
public interface Comparable<T> {
   public int compareTo(T arg0);
}
```

La declaració de les classes:

```java
    public class Data implements Comparable<Data>
```

I el mètode d'ordenació:

```java
    public static <T extends Comparable<T>> void sort(T[] vector) {
       T clau;
       int acanviar, posicio;

       for (posicio=1; posicio<vector.length; posicio++) {
           clau = vector[posicio];
           acanviar = posicio - 1;
           while (acanviar >= 0 && vector[acanviar].compareTo(clau) > 0) {
               vector[acanviar+1] = vector[acanviar];
               acanviar = acanviar - 1;
           }
           vector[acanviar+1] = clau;
       }
   }
```

Fixeu-vos que en la declaració del mètode s'especifica que es pot rebre
qualsevol tipus amb l'única condició que sigui comparable amb sí mateix,
és a dir, que implementi `Comparable<T>`, on *T* és la seva
classe.

Refinant encara una mica més, podem declarar el mètode com:

```java
    public static <T extends Comparable<? super T>> void sort(T[] vector) {
```

Aquesta declaració indica que es pot rebre qualsevol tipus *T* amb la
condició que es pugui comparar amb alguna altra classe que sigui o *T* o
una classe de la qual *T* derivi. Per exemple, si utilitzem la classe
*DataLloc*, que especifica a més d'una data, una posició geogràfica
concreta, el mateix mètode d'ordenació ens serviria per ordenar un
vector d'objectes d'aquest tipus, comparant-los com a dates.

Doncs justament, acabem de crear la interfície *Comparable* del Java i
un mètode similar al *sort* de la classe *Collections*. Fixeu-vos que la
interfície ens ha servit per indicar que una sèrie de classes que en
principi no tenien res a veure compartien una característica comuna.
Posteriorment, ens ha servit per poder realitzar operacions sobre
qualsevol objecte que, independentment de quina classe sigui, compleix
aquesta característica comuna.

La interfície *Comparable* no és suficient quan hi ha diverses formes de
comparar dos objectes d'un tipus. Imaginem per exemple la classe
*VolReal* que representa un vol concret que surt d'un aeroport cap a un
altre, amb una data concreta de sortida i una data concreta d'arribada.
De vegades ens interessarà ordenar els vols segons el moment de la seva
sortida, mentre que altres vegades serà segons la seva arribada.

En aquests casos podem fer que l'ordre donat pel *compareTo* sigui el
més habitual, i afegir un altre tipus d'ordenació, creant un objecte de
tipus *Comparator*:

```java
/*
* Retorna un nombre negatiu si aquest vol surt amb anterioritat a arg0,
* un nombre positiu si surt amb posterioritat, o 0 si surten simultàniament.
*
*/
@Override
public int compareTo(VolReal arg0) {
   return sortida.compareTo(arg0.sortida);
}

/*
* Comparador per comparar dos vols segons el seu moment d'arribada.
* Retorna positiu si arg0 arriba abans que arg1, negatiu si arriba després,
* o 0 si arriben simultàniament.
*/
public static final Comparator<VolReal> ORDRE_ARRIBADA =
       new Comparator<VolReal> () {
   @Override
   public int compare(VolReal arg0, VolReal arg1) {
       return arg0.getArribada().compareTo(arg1.getArribada());
   }
};
```

Fixeu-vos que en aquest exemple, hem creat una classe que implementa
*Comparator* de forma implícita, és a dir, sense posar-li nom, i n'hem
creat un únic objecte. Veurem més exemples d'això en el tema d'entorn
gràfic.

El mètode sort de *Collections*, per exemple, accepta un *Comparator*
com a paràmetre, si volem utilitzar un tipus d'ordre diferent de l'ordre
habitual de la classe:

```java
Collections.sort(volsReals, VolReal.ORDRE_ARRIBADA);
```

En el següent exemple de codi es poden veure diverses maneres d'implementar
la interfície Comparator per resoldre el problema dels vols, cada cop més
compacte:

[Codi d'exemple VolReal](codi/utilitzacio_avancada_de_classes/exemple_comparator)
