## Accés a una base de dades amb Eloquent

L'Eloquent és un ORM (*Object-relational mapping*) inclòs a Laravel.

Per a cada taula de la base de dades tindrem una classe model corresponent.
L'Eloquent segueix una sèrie de convencions, que es poden configurar per
casos especials que no les compleixin:

- Els noms dels models utilitzen *CamelCase* mentre que els noms de les taules
utilitzen *snake_case*.

- Els noms dels models utilitzen el singular, però els noms de les taules el
plural.

- Totes les taules tenen una clau primària anomenada *id*.

- La base de dades utilitza *timestamps*, és a dir, totes les taules tenen
les columnes *created_at* i *updated_at*.

Per exemple, si tenim una classe model anomenada *Garden*, per defecte
s'intentarà connectar amb la taula *gardens* utilitzant com a identificador
el camp *id*.

Per a crear l'esquelet d'aquest model podem utilitzar l'ordre:

```php5
php artisan make:model Garden
```

Les diverses dades del model es guarden utilitzant arrays associatius a la
classe *Model*, que és la classe base de tots els models. Això fa que no
haguem de declarar més variables ni mètodes als nostres models per tal que
siguin funcionals.

Per models complexos, podem crear mètodes que ens facilitin la gestió de les
seves o que ja tinguin algunes consultes complexes implementades. També haurem
d'afegir mètodes per definir les relacions entre classes, tal com veurem una
mica més endavant.

### Consultes simples

Les consultes a través d'Eloquent utilitzen una sintaxi fluïda independent
del SQL.

Cada mètode que cridem sobre una consulta retorna la pròpia consulta, així
que podem encadenar diverses clàusules per definir la consulta exacte que
volem fer.

Curiosament, no cal cridar cap mètode en especial per començar una consulta
nova contra un model, sinó que podem cridar qualsevol mètode de consulta
directament, encara que els mètodes en qüestió no estiguin definits a la classe
*Model*.

El misteri de com pot funcionar això es resol mirant el codi font de *Model*.
Cap al final del codi trobem el mètode *__call*:

```php5
public function __call($method, $parameters)
{
    if (in_array($method, ['increment', 'decrement'])) {
        return call_user_func_array([$this, $method], $parameters);
    }
    $query = $this->newQuery();
    return call_user_func_array([$query, $method], $parameters);
}
```

Bàsicament, el mètode *__call* és un mètode especial de PHP (tots els
mètodes començats per __ ho són, com per exemple els constructors), que es
crida sempre que intentem cridar un mètode sobre un objecte de la classe que
no existeixi.

En aquest cas podem veure com el que es fa és crear una nova consulta, i
passar el mètode desconegut a la consulta.

Si anem més a l'ús pràctic, vegem quin tipus de consultes podem fer amb
l'Eloquent.

En els següents exemples utilitzem la classe *Species*. Un objecte d'aquesta
classe guarda informació sobre una espècies de planta.

Per obtenir totes les espècies de la base de dades:

```php5
$species = Species::all();
```

Per recórrer-los després:

```php5
foreach ($species as $sp) {
  echo $sp->name;
}
```

Per obtenir una espècie, coneixent-te el seu nom:

```php5
$sp = Species::where('name', 'Fast Fourier')->firstOrFail();
$sp = Species::where('name', 'Fast Fourier')->first();
```

La primera crida farà que el programa llenci una excepció del tipus
*ModelNotFoundException* si l'espècie seleccionada no
existeix, mentre que la segona retornarà *null*.

Si estem cercant per l'*id*:

```php5
$sp = Species::find(3);
```

Si no volem tota la fila, també podem consultar camps concrets:

```php5
$name = Species::find(3)->value('name');
```

També tenim mètodes per recuperar agregacions. Per exemple, per veure la
quantitat total d'espècies que hi ha:

```php5
$nSpecies = Species::count();
```

O la quantitat d'espècies que necessiten almenys 5 unitats d'aigua per
sobreviure:

```php5
$nSpecies = Species::where('minWater', '>=', 5)->count();
```

A banda de fer consultes sobre models, també en podem fer encara que no
tinguem un model associat a una taula de la base de dades.

En aquest cas podem utilitzar el mètode `DB::table()` passant-li el nom
de la taula, i a continuació fer qualsevol de les consultes anteriors:

```php5
$nSpecies = DB::table('species')->count();
```

### Modificació i inserció de dades

La modificació o inserció de dades a través d'un model és força senzilla. En
cas de voler modificar una fila existent, primer l'hem d'obtenir de la base
de dades amb una consulta com les anteriors; si volem crear una fila nova
hem de crear un objecte del model corresponent amb *new*.

A partir d'aquí, actualitzem o completem tots els camps de l'objecte que volem
modificar o inserir i, per fer efectius els canvis a la base de dades, cridem
el seu mètode *save()*.

En el següent exemple, seleccionem una de les espècies existents i li
modifiquem algunes dades:

```php5
$sp = Species::find(3);
$sp->name = 'Amanita Muscaria';
$sp->minWater = 3;
$sp->save();
```

Si només volem actualitzar dades d'algunes files, no és necessari que les
recuperem com a objectes, ho podem fer directament en una sola consulta:

```php5
Species::where('minWater', 2)->update(['minWater'=>3, 'maxWater'=>5]);
```

### Eliminació de dades

L'eliminació també és senzilla: podem eliminar la fila corresponent a un
objecte model que tinguem, o podem elminar una o més files directament en
una consulta.

Esborrem a partir d'un objecte model:

```php5
$sp = Species::find(3);
$sp->delete();
```

Esborrem una o més files a través de les seves claus primàries:

```php5
Species::destroy(1, 2, 3);
```

O esborrem a través d'una consulta:

```php5
$affectedRows = Species::where('minWater', '<', 2)->delete();
```

### Àmbits locals

Els **àmbits locals** (*local scopes*) permeten reutilitzar parts de consultes
freqüents.

Imaginem que tenim un model per a plantes concretes, i que és habitual
consultar quines plantes tenen un nivell d'aigua entre dos valors. Podríem
crear el següent mètode a *Plant*:

```php5
public function scopeWaterBetween($query, $min, $max) {
    return $query->where('water', '>=', $min)->where('water', '<=', $max);
}
```

Per tal que un mètode es pugui utilitzar com un àmbit local cal que el seu
nom comenci amb la paraula *scope*.

Un cop definit el mètode anterior, el podem utilitzar com a part d'altres
consultes:

```php5
$plants = Plant::waterBetween(3, 8)->get();
```

Això ens retornarà totes les plantes que tinguin un nivell d'aigua entre 3 i 8.
La crida que s'ha de fer correspon amb el nom del mètode, traient la paraula
*scope* i posant a minúscules la primera lletra.
