**POO i accés a dades**

M6UF1. Persistència en fitxers
==============================

Operacions sobre fitxers
------------------------

* [Interfície *Path*](path.md)

Lectura i escriptura de fitxers binaris
---------------------------------------

* [Lectura/escriptura byte a byte](fitxers_bytes.md)
* [Lectura/escriptura de dades binàries](fitxers_dades.md)
* [Lectura/escriptura d'objectes](fitxers_objectes.md)
* [Fitxers d'accés aleatori](acces_aleatori.md)

Lectura i escriptura de fitxers de text
---------------------------------------

* [Lectura/escriptura de text caràcter a caràcter](fitxers_caracters.md)
* [Lectura/escruptura de text línia a línia](fitxers_linies.md)

Resum
-----

* [Quadre resum de classes relacionades amb fitxers](resum_classes.md)

Lectura i escriptura de fitxers XML
-----------------------------------

* [Lectura/escriptura utilitzant DOM](fitxers_xml_dom.md)
* [Lectura utilitzant SAX](fitxers_xml_sax.md)
* [Validació de documents XML](validacio_xml.md)
* [Processat de XML amb XSL](processat_xsl.md)

Exercicis
---------

* [Exercicis](exercicis.md)
* [Enunciat de l'examen](examen.md)
* 
